"""
Settings file with needed global variables and .env fetching
capabilities for variable data that should not be committed.
All project setting data should go through here if if a
PROJECT_DIRETORY/config/some_config.json file is used.


"""
import os
from dotenv import load_dotenv

# Constant variable to store the project directory
BASE_FOLDER = (os.getcwd())

# Load the environment variables from the PROJECT_DIRECTORY/.env
load_dotenv()

DEBUG = os.getenv('DEBUG')

dev_prod = 'dev' if DEBUG and DEBUG.upper() == 'TRUE' else 'prod'

# Load in the correct settings for the mode you are running
# Ex. Production or Development
dev_prod_dotenv_path = os.path.join(BASE_FOLDER, f'.{dev_prod}.env')

# Load the environment variables from the PROJECT_DIRECTORY/ based
# on if the app is in production or development mode
load_dotenv(dev_prod_dotenv_path)

# ___________________________________________________________________
# Add custom code below this point
# Example:
# SOME_VAR = os.get_env('SOME_VAR')
# NOTE: SOME_VAR should be present in the .dev.env and or the
#      .prod.env file(s)
# ___________________________________________________________________

if DEBUG:
    PARAM_LIST = [
                ["Note", "First Line"],
                ["Info","Feat ID"],
                ["Info","Feat #"],
                ["Model Params","ITEMCODE"],
                ["Model Params","COUNTED_QTY"],
                ["Model Params","QTY"],
                ["Model Params", "JOB"],
                ["Model Params", "WT"],
                ["Model Params", "MATERIAL"],
                ["Model Params", "THICKNES"],
                ["Model Params", "GRAINDIRECTION"],
                ["Model Params", "JIG"],
                ["Model Params", "POWDER"],
                ["Model Params", "EARTH_TP"]]

# ___________________________________________________________________