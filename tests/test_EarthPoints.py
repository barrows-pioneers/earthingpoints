from ..EarthPoints import EarthingPoints

import json
import os

def test_EarthingPoints():

    def json_reader(file):
        with open(file) as json_file:
            allparts = json.load(json_file)
        return allparts
    
    output_path = ".\\output"
    path=".\\testing_data"
    allparts = json_reader(os.path.join(path, "allparts_sample.json"))   
    filename = list(allparts.keys())[0].split(".")[0][:-4] +"_EarthTP.pdf"
    pdf_file = os.path.join(output_path, filename)
    if os.path.exists(pdf_file):
        try:
            os.remove(pdf_file)
        except: 
            pass
    

    data = {
            'allparts' : {
                'type' : dict,
                'value' : allparts,
            },
            'asmname' : {
                'type' : str,
                'value' : list(allparts.keys())[0],
            },
            'assemblies' : {
                'type': dict,
                'value' : json_reader(os.path.join(path, "asmDict_sample.json")),
            },
            'file_location' : {
                'type' : str,
                'value' : pdf_file,
            },
        }

    EarthingPoints(data)
    
    assert os.path.exists(pdf_file)

def test_EarthingPoints_family_table():
    
    def json_reader(file):
        with open(file) as json_file:
            allparts = json.load(json_file)
        return allparts
    
    output_path = ".\\output"
    path=".\\testing_data"
    allparts = json_reader(os.path.join(path, "allparts_family_table.json"))   
    filename = list(allparts.keys())[0].split(".")[0][:-4] +"_EarthTP.pdf"
    pdf_file = os.path.join(output_path, filename)
    if os.path.exists(pdf_file):
        try:
            os.remove(pdf_file)
        except: 
            pass
    

    data = {
            'allparts' : {
                'type' : dict,
                'value' : allparts,
            },
            'asmname' : {
                'type' :str,
                'value' : list(allparts.keys())[0],
            },
            'assemblies' : {
                'type': dict,
                'value': json_reader(os.path.join(path, "asmDict_family_table.json")),
            },
            'file_location' : {
                'type' : str,
                'value' : pdf_file,
            },
        }

    EarthingPoints(data)
    
    assert os.path.exists(pdf_file)


def test_EarthingPoints_large_asm_level():

    def json_reader(file):
        with open(file) as json_file:
            allparts = json.load(json_file)
        return allparts

    output_path = ".\\output"
    path = ".\\testing_data"
    allparts = json_reader(os.path.join(path, "allparts-large_asm_level.json"))
    filename = list(allparts.keys())[0].split(".")[0][:-4] + "_EarthTP.pdf"
    pdf_file = os.path.join(output_path, filename)
    if os.path.exists(pdf_file):
        try:
            os.remove(pdf_file)
        except:
            pass

    data = {
        'allparts': {
            'type': dict,
            'value': allparts,
        },
        'asmname': {
            'type': str,
            'value': list(allparts.keys())[0],
        },
        'assemblies': {
            'type': dict,
            'value': json_reader(os.path.join(path, "asmDict-large_asm_level.json")),
        },
        'file_location': {
            'type': str,
            'value': pdf_file,
        },
    }

    EarthingPoints(data)

    assert os.path.exists(pdf_file)
