# Project Name #

Project description

## Technologies used

* Python x.x
* ...

#### **NOTE:** ####
  - **All commands should be run in the project root directory**

## Prerequisites ##

* Install virtualenv globally
```
pip install virtualenv
```

## How do I get set up? ##

* Create the repository on the remote (for example via Bitbucket)

* Initialize local repository
  ```
  git init
  ```

* Link the remote up with the local repository
  ```
  git remote add origin git@bitbucket.org:<repo_owner_username>/<repository>.git
  ```
* Commit all files

* Then push the initial commit up to remote, using the master branch
  ```
  git push -u origin master
  ```

* Create virtual environment
  ```
  virtualenv -p x .virtualenv
  ```


* Add your pip packages to PROJECT_DIRECTORY/requirements.txt

* And activate your newly created virtual environment
  ```
  PROJECT_DIRECTORY/.virtualenv/Scripts/activate
  ```

* Make sure VSCode's interpreter for python is set to the newly created
  virtual environment, and that the environment is active in the terminal.
  You should see a ```(.virtualenv)``` prefix in the terminal.

* Then install the packages needed there
  ```
  pip install -r PROJECT_DIRECTORY/requirements.txt
  ```

* Add your environment variables into the .dev_prod.env.template (WITHOUT VALUES, Just the variable names as a template)

* Copy PROJECT_DIRECTORY/.env.template to the root directory with the name .env

* Copy PROJECT_DIRECTORY/.dev_prod.env.template to the root directory with the name .dev.env and or .prod.env (This is where your production and development environment variables will be set)

* Fill in the variables needed in the files created above. DEBUG="TRUE" should be the only variable present in the .env file in the root of the project directory

* Next fill in the PROJECT_DIRECTORY/settings.py file if needed (Read the docstring and comments in the settings file)

---

* **Contact me by below details if any clarification is needed. Happy devving!**

## Install Subtree Libraries ##

Installation of subtree libraries should only be installed once

* **Adding** the subtree library
  ```
  git subtree add --prefix libraries/shared/<library_name> git@bitbucket.org:<repo_owner_username>/<library_name>.git <branch> --squash

  git subtree add --prefix libraries/shared/pioneering_utilities git@bitbucket.org:barrows-pioneers/pioneering_utilities.git master --squash
  ```

* **Updating** the library can be done via the following command
  ```
  git subtree pull --prefix libraries/shared/<library_name> git@bitbucket.org:<repo_owner_username>/<library_name>.git <branch> --squash
  ```

* Lastly the requirements of the subtree module can be added to the root requirements file. Add the below line to that file
  ```
  -r ./libraries/shared/<library_name>/requirements.txt
  ```

## Contribution guidelines ##

* Branch of off the dev/master branch

* Make your changes, commit and push to your custom branch

* Create a pull request into the dev/master branch for review

* Make sure code is at least 80% unit tested

* Make sure that documentation has been generated

* See below naming conventions:
  * ```ClassFileName```
  * ```ClassName```
  * ```variable_name```
  * ```repository_name```
  * ```folder_name```

* Remove any unneeded folder structures or files

## Testing ##

* Running pytest unit tests manually
```
pytest ./
```

* Pytest coverage report in ./coverage directory (shared libraries are ignored for unit testing)

* For debugging tests, refer to the comment in the root file called pytest.ini

### Documenting ###

* Generating documentation (shared libraries are ignored)
```
pdoc --html ./ --output-dir docs --force
```

* The newly generated documentation should now be in the root folder called docs/

* This documentation is generated using the docstrings added in the python files

## Sub library Guidelines ##

* Classes can go into the root directory instead of having an app.py file

## Folder Structure Information ##
* ```./binaries/```
  * Place to store generated binary files
* ```./config/```
  * Place to store project specific configuration files
* ```./libraries/```
  * Contains groups of modules for use in the project
* ```./libraries/shared```
  * Contains groups of modules for use in the project, with the exception of having their own repositories and being shared
* ```./output/```
  * Place to store any direct file output from the project
* ```./temp_files/```
  * Place to store any developer specific snippets
* ```./testing_data/```
  * Contains any data and directories needed for testing
* ```./tests/```
  * Contains the test files for the code in the directory of the folder
* ```./models/```
  * Contains any machine learning models that you want to share with the repository

## Who do I talk to? ##

* Hein Gericke (Contributor) - <hein.gericke@barrowsglobal.com>
* Martin Cronje (Contributor) - <martin.cronje@barrowsglobal.com>
* Grant Purdon (Creator) - <grant.purdon@barrowsglobal.com>

---

#### NOTICE: This is a Barrows Global (https://www.barrowsglobal.com/) software product ####