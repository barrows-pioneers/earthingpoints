from fpdf import FPDF

class EarthingPoints():
    def __init__(self, data) -> None:


        self.allparts = data['allparts']['value']
        self.asmname = data['asmname']['value']
        self.file_location = data['file_location']['value']
        self.assemblies = data['assemblies']['value']
        self.asm_list_elements = self.asm_list()
        if len(self.parts_list()) > 0:
            try:
                self.pdf_earth_tp()
            except PermissionError as error:
                raise PermissionError(error)


    def parts_list(self):
        """
        Finds and returns parts with Earth Test Points in them.
        """
        parts=[]
        for part in self.allparts:
            if self.allparts[part]["EARTH_TP"].lower() == 'yes':
                parts.append(part)
        return parts

    def remove_generic_info(self,key):
        temp = 0
        if str(key).find('<')>-1:
           temp = key[:str(key).find('<')] + key[str(key).find('>')+1:]
        else:
           temp=key   
        return temp

    def get_asm_tree(self,parts):
        """
        Make smaller dictionary of only assemblies/subassemblies "Axx" as the key
        """
        assemblies_list = {self.remove_generic_info(key):val for key, val in self.assemblies.items() if (str(key).find("-A")>-1) }
        a_list = [] 
        # test_dict = [key for key, val in self.assemblies.items() if (parts in val)] #issues in comprehension
        # assign first key/value pair
        temp = parts    
        a_list.append(temp)
        for x in range(len(list(assemblies_list.keys()))):
            temp = "".join([key for key, val in self.assemblies.items() if (temp in val)])
            if len(temp)>0:
                a_list.append(temp)
    
        a_list.reverse()
                                                                       
        return a_list

    def asm_list(self):
        #returns printable model tree list showing path to each Earth Test Point 
        as_list = []   
        #list  earth test point parts
        prt_list = self.parts_list()
        #iterate through EarthTP parts  and build model tree to these parts
        for part in prt_list: 
            as_list.append(self.get_asm_tree(part))
        return as_list
    

    def pdf_earth_tp(self):
        pdf = FPDF()

        pdf.add_page()

        pdf.set_font("Arial",style='B', size = 16)
        pdf.cell(140, h=8, ln=0, txt="Earthing Test Points", border=1)

        pdf.set_font("Arial", style='B', size=12)
        pdf.cell(50, h=8, ln=1, txt="UNIT QTY:" + self.allparts[self.asmname]["QTY"], align='R',border=1)

        pdf.set_font("Arial", style='B', size=12)
        pdf.cell(190, h=7, ln=1, txt="Description: " + self.allparts[self.asmname]["DESCRIPTION"], align='L',border=1)
        
        pdf.set_font("Arial", style='B', size=12)
        pdf.cell(190, h=6, ln=1, txt="Job: " + self.allparts[self.asmname]["JOB"] + "-" + self.allparts[self.asmname]["WT"], align='L', border=1)
        pdf.set_font("Arial", style='BI', size=12)
        pdf.cell(75, h=6, ln=0, txt="Note: Earthing test point pass = <0.1", align='L', border='LBT')

        pdf.set_font("Symbol", style='B', size=12)
        pdf.cell(115, h=6, ln=1, txt="\x57", border='RBT')

        pdf.ln(9)

        pdf.set_font("Arial",style='B', size=11)
        pdf.cell(10, h=6, ln=0, txt="NOS", align='C', border=1)
        pdf.cell(180, h=6, ln=1, txt="PART NUMBER", align='L', border=1)

        pdf.set_font("Arial", style='', size=11)
        flag = False    
        nos=1
        tab=0
        for asm in self.asm_list_elements:
            # display line number for the Earth Test point only
            pdf.cell(10, h=6, ln=0, txt=str(nos), align='C', border=1)
            tab = 0
            for earth_part in asm:
                # display space only for consecutive lines    
                if flag:  
                    pdf.cell(10, h=6, ln=0, txt=" ", align='C', border=1)
                else:
                    flag = True    
                #display EarthPart line without file extensions ".ASM" or ".PRT"   
                pdf.cell(180, h=6, ln=1, txt=(tab * "   ") + earth_part[:-4],align='L',border=1)
                tab += 1
            flag = False    
            nos += 1
        #output PDF to .\output\
    
        pdf.output(name= self.file_location, dest='f')
    
